package com.fetch.common.messaging;

import java.util.function.Consumer;

/**
 * A components that forwards a batch of events to an event processor.
 *
 * Notably, this component can facilitate at-least-once semantics for event processing when: (1) events are retrieved
 * from a persistence mechanism that supports ACID transactions, and (2) {@link #forward(Consumer)} executes within a
 * transactional boundary.
 *
 * @param <BatchT>
 */
public interface BatchMessageForwarder<BatchT> {
    void forward(Consumer<BatchT> processor);
}
