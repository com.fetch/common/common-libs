package com.fetch.common.messaging;

import java.util.function.Consumer;

public interface BatchMessagePublisher<BatchT> extends Consumer<BatchT> {
    void publish(BatchT batch);

    @Override
    default void accept(BatchT batch) {
        publish(batch);
    }
}
