package com.fetch.common.messaging;

/**
 * A stateless service that propagates a perpetual log of events to a pub/sub messaging system. Events are forwarded
 * to a publisher one batch at a time; the publisher is responsible for sending each batch to the message broker.
 *
 * @param <BatchT> The Batched Event type.
 */
public abstract class AbstractMessagePublishingService<BatchT> {

    /**
     * Returns an component that handles the responsibility of forwarding events.
     */
    abstract protected BatchMessageForwarder<BatchT> forwarder();

    /**
     * Returns a publisher for a concrete pub/sub messaging system (e.g., Kafka).
     */
    abstract protected BatchMessagePublisher<BatchT> publisher();

    public void publish() {
        // Forward the next batch to the message publisher.
        forwarder().forward(publisher());
    }
}
