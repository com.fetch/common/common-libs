package com.fetch.common.notifications;

import com.fetch.common.validate.Validate;

public class Entity {
    private String name;

    public static Entity ofName(String name) {
        return new Entity(name);
    }

    Entity(String name) {
        setName(name);
    }

    public String name() {
        return this.name;
    }

    private void setName(String name) {
        Validate.notNull(name, "The entity's name cannot be null.");
        this.name = name;
    }
}
