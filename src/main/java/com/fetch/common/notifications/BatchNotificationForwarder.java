package com.fetch.common.notifications;

import com.fetch.common.validate.Validate;
import com.fetch.common.messaging.BatchMessageForwarder;
import org.apache.commons.lang3.mutable.MutableObject;
import org.springframework.transaction.annotation.Transactional;

import java.util.function.Consumer;
import java.util.stream.Stream;

public class BatchNotificationForwarder implements BatchMessageForwarder<Stream<Notification>> {
    private NotificationStore notificationStore;
    private NotificationTracker notificationTracker;

    public BatchNotificationForwarder(NotificationStore notificationStore, NotificationTracker notificationTracker) {
        Validate.notNull(notificationStore, "The notification store cannot be null.");
        Validate.notNull(notificationTracker, "The notification tracker cannot be null.");
        this.notificationStore = notificationStore;
        this.notificationTracker = notificationTracker;
    }

    @Override
    @Transactional
    public void forward(Consumer<Stream<Notification>> processor) {
        NotificationId notificationId = notificationTracker.mostRecentlyPublishedNotificationId();
        Stream<Notification> notifications = notificationStore.notificationsAfterId(notificationId);

        MutableObject<NotificationId> mostRecentNotificationIdTracker = new MutableObject<>();
        processor.accept(notifications.peek(notification -> mostRecentNotificationIdTracker.setValue(notification.getId())));
        NotificationId mostRecentNotificationId = mostRecentNotificationIdTracker.getValue();

        if (mostRecentNotificationId != null) {
            notificationTracker.recordMostRecentlyPublishedNotificationId(mostRecentNotificationId);
        }
    }

    public NotificationStore getNotificationStore() {
        return notificationStore;
    }
}
