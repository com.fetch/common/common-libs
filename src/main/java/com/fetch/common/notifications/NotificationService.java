package com.fetch.common.notifications;

import com.fetch.common.messaging.AbstractMessagePublishingService;
import com.fetch.common.messaging.BatchMessageForwarder;
import com.fetch.common.messaging.BatchMessagePublisher;

import java.util.stream.Stream;

public class NotificationService extends AbstractMessagePublishingService<Stream<Notification>> {
    private BatchNotificationPublisher publisher;
    private BatchNotificationForwarder forwarder;

    public NotificationService(BatchNotificationPublisher batchNotificationPublisher, BatchNotificationForwarder batchNotificationForwarder) {
        this.publisher = batchNotificationPublisher;
        this.forwarder = batchNotificationForwarder;
    }

    @Override
    protected BatchMessagePublisher<Stream<Notification>> publisher() {
        return publisher;
    }

    @Override
    protected BatchMessageForwarder<Stream<Notification>> forwarder() {
        return forwarder;
    }

    public void save(DomainEvent domainEvent) {
        NotificationId notificationId = forwarder.getNotificationStore().nextIdentity();
        Notification notification = new Notification(notificationId, domainEvent);
        forwarder.getNotificationStore().append(notification);
    }
}
