package com.fetch.common.notifications;

import com.fetch.common.validate.Validate;

public class DomainEventGroup {
    private Domain domain;
    private Entity entity;

    public static DomainEventGroup of(Domain domain, Entity entity) {
        return new DomainEventGroup(domain, entity);
    }

    DomainEventGroup(Domain domain, Entity entity) {
        setDomain(domain);
        setEntity(entity);
    }

    public Domain domain() {
        return this.domain;
    }

    public Entity entity() {
        return this.entity;
    }

    private void setDomain(Domain domain) {
        Validate.notNull(domain, "The groups domain cannot be null.");
        this.domain = domain;
    }

    private void setEntity(Entity entity) {
        Validate.notNull(entity, "The group's entity cannot be null.");
        this.entity = entity;
    }
}
