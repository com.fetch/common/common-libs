package com.fetch.common.notifications;

import java.util.stream.Stream;

public interface SimpleNotificationPublisher extends BatchNotificationPublisher {
    @Override
    default void publish(Stream<Notification> batch) {
        batch.forEach(this::publish);
    }

    void publish(Notification notification);
}
