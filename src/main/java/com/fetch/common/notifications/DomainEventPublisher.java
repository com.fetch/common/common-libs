package com.fetch.common.notifications;

import java.util.ArrayList;
import java.util.List;

public class DomainEventPublisher {
    private static final ThreadLocal<List> subscribers = new ThreadLocal<>();
    private static final ThreadLocal<Boolean> publishing = ThreadLocal.withInitial(() -> Boolean.FALSE);

    public static DomainEventPublisher instance() {
        return new DomainEventPublisher();
    }

    public DomainEventPublisher() {
        super();
    }

    public <T> void publish(final DomainEvent<T> domainEvent) {
        if (publishing.get()) {
            return;
        }
        try {
            publishing.set(Boolean.TRUE);
            List<DomainEventSubscriber> registeredSubscribers = subscribers.get();
            if (registeredSubscribers != null) {
                Class eventType = domainEvent.getEvent().getClass();
                for (DomainEventSubscriber subscriber: registeredSubscribers) {
                    Class subscribedEventType = subscriber.subscribedEventType();
                    if (subscribedEventType.isAssignableFrom(eventType)) {
                        subscriber.handleEvent(domainEvent);
                    }
                }
            }
        } finally {
            publishing.set(Boolean.FALSE);
        }
    }

    public DomainEventPublisher reset() {
        if (!publishing.get()) {
            subscribers.set(null);
        }
        return this;
    }

    public <T> void subscribe(DomainEventSubscriber<T> subscriber) {
        if (publishing.get()) {
            return;
        }
        List<DomainEventSubscriber<T>> registeredSubscribers = subscribers.get();
        if (registeredSubscribers == null) {
            registeredSubscribers = new ArrayList<>();
            subscribers.set(registeredSubscribers);
        }
        registeredSubscribers.add(subscriber);
    }
}
