package com.fetch.common.notifications;

import com.fetch.common.validate.Validate;

import java.io.Serializable;

public class Notification implements Serializable {
    private NotificationId id;
    private DomainEvent event;

    Notification(NotificationId id, DomainEvent event) {
        setId(id);
        setEvent(event);
    }

    private void setId(NotificationId id) {
        Validate.notNull(id, "The id may not be null.");
        this.id = id;
    }

    private void setEvent(DomainEvent event) {
        Validate.notNull(event, "The event may not be null.");
        this.event = event;
    }

    public NotificationId getId() {
        return id;
    }

    public DomainEvent getEvent() {
        return event;
    }
}
