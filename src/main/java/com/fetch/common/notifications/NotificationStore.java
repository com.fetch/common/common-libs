package com.fetch.common.notifications;

import java.util.stream.Stream;

public interface NotificationStore {
    NotificationId nextIdentity();
    Stream<Notification> notificationsAfterId(NotificationId id);
    void append(Notification notification);
}
