package com.fetch.common.notifications;

public interface DomainEventSubscriber<T> {
    void handleEvent(final DomainEvent<T> domainEvent);
    Class<T> subscribedEventType();
}
