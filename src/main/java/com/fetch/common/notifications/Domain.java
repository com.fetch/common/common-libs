package com.fetch.common.notifications;

import com.fetch.common.validate.Validate;

public class Domain {
    private String name;

    public static Domain ofName(String name) {
        return new Domain(name);
    }

    Domain(String name) {
        setName(name);
    }

    public String name() {
        return name;
    }

    private void setName(String name) {
        Validate.notNull(name, "The domain's name cannot be null.");
        this.name = name;
    }
}
