package com.fetch.common.notifications.spring;

import com.fetch.common.notifications.*;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public abstract class AbstractNotificationConfiguration {
    protected abstract BatchNotificationPublisher notificationPublisher();

    @Bean
    public NotificationService notificationService(BatchNotificationForwarder notificationForwarder) {
        return new NotificationService(notificationPublisher(), notificationForwarder);
    }

    @Bean
    public BatchNotificationForwarder notificationForwarder(NotificationStore notificationStore, NotificationTracker notificationTracker) {
        return new BatchNotificationForwarder(notificationStore, notificationTracker);
    }
}
