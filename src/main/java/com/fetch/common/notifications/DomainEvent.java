package com.fetch.common.notifications;

public interface DomainEvent<T> {
    DomainEventGroup getGroup();
    T getEvent();
}
