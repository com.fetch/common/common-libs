package com.fetch.common.notifications;

import com.fetch.common.messaging.BatchMessagePublisher;

import java.util.stream.Stream;

public interface BatchNotificationPublisher extends BatchMessagePublisher<Stream<Notification>> {
}
