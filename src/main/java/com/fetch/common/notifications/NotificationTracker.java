package com.fetch.common.notifications;

public interface NotificationTracker {
    NotificationId mostRecentlyPublishedNotificationId();
    void recordMostRecentlyPublishedNotificationId(NotificationId mostRecentlyPublishedNotificationId);
}
