package com.fetch.common.notifications;

import com.fetch.common.validate.Validate;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

public class NotificationId {
    private String id;

    public static NotificationId of(String id) {
        return new NotificationId(id);
    }

    private NotificationId(String id) {
        setId(id);
    }

    private void setId(String id) {
        Validate.notNull(id, "The notification ID cannot be null.");
        this.id = id;
    }

    public String getId() {
        return id;
    }

    @Override
    public String toString() {
        return getId();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        NotificationId that = (NotificationId) o;

        return new EqualsBuilder()
                .append(id, that.id)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(id)
                .toHashCode();
    }
}
