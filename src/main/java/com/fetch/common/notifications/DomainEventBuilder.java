package com.fetch.common.notifications;

import com.fetch.common.validate.Validate;

public class DomainEventBuilder {
    private DomainEventGroup group;
    private Object event;

    public static DomainEventBuilder forGroup(DomainEventGroup group) {
        return new DomainEventBuilder(group);
    }

    DomainEventBuilder(DomainEventGroup group) {
        this.group = group;
    }

    public <T> DomainEventBuilder withEvent(T event) {
        this.event = event;
        return this;
    }

    public DomainEvent<?> build() {
        Class<?> eventClass = event.getClass();
        return new DomainEventImpl<>(group, eventClass.cast(event));
    }

    static class DomainEventImpl<T> implements DomainEvent<T> {
        private DomainEventGroup group;
        private T event;

        DomainEventImpl(DomainEventGroup group, T event) {
            setGroup(group);
            setEvent(event);
        }

        private void setGroup(DomainEventGroup group) {
            Validate.notNull(group, "The group cannot be null.");
            this.group = group;
        }

        private void setEvent(T event) {
            Validate.notNull(event, "The event cannot be null.");
            this.event = event;
        }

        @Override
        public DomainEventGroup getGroup() {
            return group;
        }

        @Override
        public T getEvent() {
            return event;
        }
    }

}
