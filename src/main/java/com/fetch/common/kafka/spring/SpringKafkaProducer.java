package com.fetch.common.kafka.spring;

import com.fetch.common.kafka.KafkaNotificationPublisher;
import org.springframework.kafka.core.KafkaOperations;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.transaction.annotation.Transactional;

public class SpringKafkaProducer implements KafkaNotificationPublisher.Producer {
    protected KafkaTemplate<Object, Object> template;

    public SpringKafkaProducer(KafkaTemplate<Object, Object> template) {
        this.template = template;
    }

    @Override
    public void send(String topic, String key, Object data) {
       send(template, topic, key, data);
    }

    protected static KafkaOperations send(KafkaOperations<Object, Object> operations, String topic, String key, Object data) {
        // TODO: Handle success and exception better
        operations.send(topic, key, data).addCallback(
                result -> System.out.println("Event sent: " + result.toString()),
                throwable -> System.out.print("Event delivery failed: " + throwable)
        );
        return operations;
    }
}
