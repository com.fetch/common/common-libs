package com.fetch.common.kafka.spring;

import org.springframework.kafka.core.KafkaTemplate;

public class SpringKafkaTransactionalProducer extends SpringKafkaProducer {
    public SpringKafkaTransactionalProducer(KafkaTemplate<Object, Object> kafkaTemplate) {
        super(kafkaTemplate);
    }

    @Override
    public void send(String topic, String key, Object data) {
        template.executeInTransaction(t -> super.send(t, topic, key, data));
    }
}
