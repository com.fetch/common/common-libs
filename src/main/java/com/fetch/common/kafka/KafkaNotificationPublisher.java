package com.fetch.common.kafka;

import com.fetch.common.notifications.DomainEvent;
import com.fetch.common.notifications.Notification;
import com.fetch.common.notifications.SimpleNotificationPublisher;

public class KafkaNotificationPublisher implements SimpleNotificationPublisher {
    private Producer producer;

    public KafkaNotificationPublisher(Producer producer) {
        this.producer = producer;
    }

    @Override
    public void publish(Notification notification) {
        DomainEvent event = notification.getEvent();
        String topic = KafkaUtils.topicForDomainEvent(notification.getEvent());
        producer.send(topic, notification.getId().toString(), event.getEvent());
    }

    public interface Producer {
        void send(String topic, String key, Object data);
    }
}
