package com.fetch.common.kafka;

import com.fetch.common.notifications.DomainEvent;
import com.fetch.common.notifications.DomainEventGroup;

class KafkaUtils {
    private static final String DOMAIN_EVENT_TOPIC_TEMPLATE = "domain.%s.%s";

    static String topicForDomainEvent(DomainEvent domainEvent) {
        DomainEventGroup group = domainEvent.getGroup();
        String topic = String.format(DOMAIN_EVENT_TOPIC_TEMPLATE, group.domain().name(), group.entity().name());
        return topic;
    }
}
