package com.fetch.common.validate;

public class Validate {

    public static void isTrue(final boolean expression, final String errorMessage) {
        org.apache.commons.lang3.Validate.isTrue(expression, errorMessage);
    }

    public static void validState(final boolean expression, final String errorMessage) {
        org.apache.commons.lang3.Validate.validState(expression, errorMessage);
    }

    /**
     * Validates that the given Object value is null.
     * @param value Object value to validate. Must be null.
     * @param errorMessage Message to throw in IllegalStateException if conditions aren't met.
     * @return
     */
    public static void stateIsNull(final Object value, final String errorMessage) {
        validState(value == null, errorMessage);
    }

    /**
     * Validates that the given Object value is non-null.
     * @param value Object value to validate. Must be non-null.
     * @param errorMessage Message to throw in IllegalArgumentException if conditions aren't met.
     * @return
     */
    public static void notNull(final Object value, final String errorMessage) {
        isTrue(value != null, errorMessage);
    }
}
